<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() :void
    {
        Schema::create('feedback', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id')->comment('ид клиента')->nullable();
            $table->string('theme')->comment('тема')->nullable();
            $table->text('messages')->comment('сообщение')->nullable();
            $table->string('link')->comment('ссилка на файл')->nullable();
            $table->integer('show_manager')->comment('просмотрено менеджером')->default(0);
            $table->timestamps();
            $table->timestamp('deleted_at')->comment('удаление')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() :void
    {
        Schema::dropIfExists('feedback');
    }
}
