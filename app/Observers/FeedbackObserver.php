<?php

declare(strict_types=1);

namespace App\Observers;

use App\Feedback;
use App\Jobs\SendEmailJob;

class FeedbackObserver
{
    /**
     * @param Feedback $model
     */
    public function created(Feedback $model): void
    {
        dispatch(new SendEmailJob($model));
    }
}
