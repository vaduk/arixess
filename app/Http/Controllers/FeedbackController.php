<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Feedback;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class FeedbackController
{
    public const FILE_PATH = '/images';

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add()
    {
        $feedback = Feedback::whereDate('created_at', '=', date('Y-m-d'))->find(auth()->user()->id);
        return view('feedback.add', compact('feedback'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function store(Request $request)
    {
        $feedback = Feedback::whereDate('created_at', '=', date('Y-m-d'))->find(auth()->user()->id);
        if(empty($feedback)) {
            $urlFile = null;
            if ($request->hasFile('file') && $request->file('file')->isValid()) {

                $file = $request->file('file');
                $urlFile = $this->upload($file);
            }
            $request['link'] = $urlFile;
            $request['client_id'] = auth()->user()->id;
            Feedback::create($request->all());
            return redirect()->route('home');
        }else{
            return view('feedback.add', compact('feedback'));
        }
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {
        $feedbacks = Feedback::
            leftjoin('users', 'users.id', '=', 'feedback.client_id')
            ->where('show_manager', 0)
            ->get();
        return view('feedback.index', compact('feedbacks'));
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function view(Request $request)
    {
        Feedback::where('id', (int)$request->post('id'))->update(['show_manager' => 1]);
        return true;
    }


    /**
     * @param $path
     * @param UploadedFile $file
     * @return string
     */
    public function upload(UploadedFile $file): string
    {
        $path = public_path() . self::FILE_PATH;
        $filename = time() . '_' . $file->getClientOriginalName();
        $file->move($path, $filename);

        return self::FILE_PATH . '/'. $filename;
    }

}
