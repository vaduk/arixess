<?php

declare(strict_types=1);

namespace App\Jobs;

use App\Feedback;
use App\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $feedback;

    /**
     * SendEmailJob constructor.
     * @param Feedback $feedback
     */
    public function __construct(Feedback $feedback)
    {
        $this->feedback = $feedback;
    }

    /**
     * @return string
     */
    public function handle()
    {
        $manager = User::whereHas(
            'roles', function($q){
            $q->where('slug', 'manager');
        }
        )->first();

        $toName = $manager->name;
        $toEmail = $manager->email;
        $data = array('name'=>auth()->user()->name, "body" => "Создан отзыв");

        Mail::send('mail', $data, function($message) use ($toName, $toEmail) {
            $message->to($toEmail, $toName)
                ->subject("Отзыв");
        $message->from(auth()->user()->email);
        });

    return 'Email sent Successfully';
    }
}
