@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <table class="table table-active">
                    <tr>
                        <th>ID</th>
                        <th>тема</th>
                        <th>сообщение</th>
                        <th>имя клиента</th>
                        <th>почта клиента</th>
                        <th>ссылка </th>
                        <th>Дата</th>
                        <th>Отметить</th>
                    </tr>
                @foreach($feedbacks as $feedback)
                    <tr>
                        <td>{{$feedback->id}}</td>
                        <td>{{$feedback->theme}}</td>
                        <td>{{$feedback->messages}}</td>
                        <td>{{$feedback->name}}</td>
                        <td>{{$feedback->email}}</td>
                        <td>{{$feedback->link}}</td>
                        <td>{{date('Y-m-d', strtotime($feedback->link))}}</td>
                        <td>
                            <input type='checkbox' class="checkboxShow" id="{{$feedback->id}}" value="{{$feedback->id}}">
                        </td>
                    </tr>
                @endforeach
                </table>
            </div>
        </div>
    </div>
@endsection

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    $(function () {
        $('.checkboxShow').on('change', function () {
            if ($('#'+this.id).is(":checked")) {
                $.ajax({
                    type: "POST",
                    url: '/contact/view',
                    data: {id: this.id, "_token": "{{ csrf_token() }}"},
                    success: function (data) {
                        location.reload();
                    }
                });
            }

        });
    });
</script>

