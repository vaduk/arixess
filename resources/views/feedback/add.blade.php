 @extends('layouts.app')

 @section('content')
     <div class="container">
         <div class="row justify-content-center">
             <div class="col-md-12">
                 @if(empty($feedback))
                     <form action="/contact/store" method="post" enctype="multipart/form-data">
                         <input type="hidden" name="_token" value="{{ csrf_token() }}">
                         <div class="form-group has-feedback {{ $errors->has('theme') ? 'has-error' : '' }}">
                             <input type="text" name="theme" class="form-control"
                                    placeholder="Название магазина" required>
                             @if ($errors->has('theme'))
                                 <span class="help-block"><strong>{{ $errors->first('theme') }}</strong></span>
                             @endif
                         </div>

                         <div class="form-group has-messages {{ $errors->has('messages') ? 'has-error' : '' }}">
                             <textarea name="message" class="form-control"></textarea>
                             @if ($errors->has('messages'))
                                 <span class="help-block"><strong>{{ $errors->first('messages') }}</strong></span>
                             @endif
                         </div>
                         <div class="form-group">
                            <input type="file" name="file" class="form-control">
                         </div>
                         <p><input type="submit" value="Отправить"></p>
                     </form>
                 @else
                     <div>
                         Ви сегодня уже оставляли заявку
                     </div>
                 @endif
             </div>
         </div>
     </div>
 @endsection
